package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile {
	@Then("user redirected to screen homepage")
	public void user_redirected_to_screen_homepage() {
		WebUI.verifyElementVisible(findTestObject('HomePage/ProfileIcn'))
	}

	@And("user click profile icon")
	public void user_click_profile_icon() {
		WebUI.click(findTestObject('HomePage/ProfileIcn'))
	}

	@And("user click list profile button")
	public void user_click_list_profile_button() {
		WebUI.click(findTestObject('Page_Logout/profileName'))
	}

	@Then("screen profile page displayed")
	public void screen_profile_page_displayed() {
		WebUI.verifyElementVisible(findTestObject('Page_Profile/LengkapiInfoAkun_Title'))
	}

	@And("user upload image {string}")
	public void user_upload_image(String pathImage) {
		WebUI.uploadFile(findTestObject('Page_Profile/avatarProfile'), pathImage)
	}

	@And("user input name {string}")
	public void user_input_name(String name) {
		WebUI.setText(findTestObject('Page_Profile/namaField'), name)
	}

	@And("user select city {string}")
	public void user_select_city(String city) {
		WebUI.selectOptionByLabel(findTestObject('Page_Profile/kotaSelectBox'), city, false, FailureHandling.STOP_ON_FAILURE)
	}

	@And("user input address {string}")
	public void user_input_address(String address) {
		WebUI.setText(findTestObject('Page_Profile/alamatField'), address)
	}

	@And("user input phone number {string}")
	public void user_input_phone_number(String phone_number) {
		WebUI.setText(findTestObject('Page_Profile/handphoneField'), phone_number)
	}

	@And("user click button simpan")
	public void user_click_button_simpan() {
		WebUI.click(findTestObject('Page_Profile/simpanButton'))
	}

	@And("user click button back profile")
	public void user_click_button_back_profile() {
		WebUI.click(findTestObject('Page_Profile/backProfileButton'))
	}

	@Then("user stay in profile page")
	public void user_stay_in_profile_page() {
		WebUI.verifyElementVisible(findTestObject('Page_Profile/LengkapiInfoAkun_Title'))
	}
}