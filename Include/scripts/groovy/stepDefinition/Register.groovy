package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



public  class Register {
	@And("user input username {string}")
	public void user_input_username(String username) {
		WebUI.setText(findTestObject('Page_Register/usernameField'), username)
	}
	// Write code here that turns the phrase above into concrete actions
	//throw new PendingException();
	@And("user clik daftar link")
	public void user_clik_daftar_link() {
		WebUI.click(findTestObject('Page_Login/daftarLink'))
	}
	
	@And("user input emailreg {string}")
	public void user_input_emailreg(String statusEmail) {
		switch(statusEmail) {
			case "emailRandom":
					def random = new Random()
					def min = 1000
					def max = 9999
					def randomNumber = min + random.nextInt(max - min + 1)
					def emailRandom = "saskia$randomNumber@gmail.com"
					WebUI.setText(findTestObject('Page_Register/emailregister'), emailRandom)
					break;
			case "existingEmail":
					WebUI.setText(findTestObject('Page_Register/emailregister'), "giselle3010@gmail.com")
					break;
			case "invalidemail":
					WebUI.setText(findTestObject('Page_Register/emailregister'), "%%")
					break;
			default:
					println("Condition Email Not Found!")
			}
	}

	@And("user clik Daftar button")
	public void user_clik_Daftar_button() {
		WebUI.click(findTestObject('Page_Register/registerButton'))
		WebUI.delay(3)
		//		WebUI.verifyElementVisible(findTestObject('HomePage/searchField'))
	}
	// Write code here that turns the phrase above into concrete actions
	//throw new PendingException();

	@Then("user stay in register page")
	public void user_stay_in_register_page() {
		WebUI.verifyElementVisible(findTestObject('Page_Register/daftarText'))
	}
}