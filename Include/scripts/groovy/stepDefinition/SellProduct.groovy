package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import internal.GlobalVariable

public class SellProduct {
	@And("click list product icon")
	public void click_list_product_icon() {
		WebUI.click(findTestObject('Aziz_Product_Page/Link_ProductListIcon'))
	}

	@Then("redirect to sell screen product page")
	public void redirect_to_sell_screen_product_page() {
		WebUI.verifyElementVisible(findTestObject('Aziz_Product_Page/Text_Daftar Jual Saya'))
		//		WebUI.verifyElementVisible(findTestObject('Aziz_Product_Page/Text_Tambah Produk'))
	}

	@And("click button Diminati")
	public void click_button_Diminati() {
		WebUI.click(findTestObject('Aziz_Product_Page/Category_Diminati'))
	}

	@Then("displays screen product page for products that are of interest to buyers")
	public void displays_screen_product_page_for_products_that_are_of_interest_to_buyers() {
		WebUI.verifyElementVisible(findTestObject('Aziz_Product_Page/Text_Mouse Logitech MX Master 3 005'))
	}

	@And("click button Terjual")
	public void click_button_Terjual() {
		WebUI.click(findTestObject('Aziz_Product_Page/Terjual'))
	}

	@Then("there is Iphone 15 pro max")
	public void there_is_Iphone_15_promax() {
		WebUI.verifyElementVisible(findTestObject('Aziz_Product_Page/Text_Iphone 15 pro max'))
	}
}
