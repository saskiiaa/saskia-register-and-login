package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Search {
	@When("user click search field")
	public void the_screen_redirect_to_SecondHand_homepage() {
		WebUI.click(findTestObject('HomePage/searchField'))
	}

	@And("user input Product {string}")
	public void user_input_product(String search) {
		WebUI.setText(findTestObject('HomePage/searchField'), search)
	}
	@When("user click enter")
	public void user_Click_enter() {
		WebUI.sendKeys(findTestObject('HomePage/searchField'), Keys.chord(Keys.ENTER))
	}

	@Then("List Product display the product")
	public void list_Product_display_product() {
		WebUI.verifyElementVisible(findTestObject('Product_Mouse_Logitech_Page/Homepage/MouseLogitechMXTxtHomapage'))
	}	
	@And("user scroll down until Mouse Logitech MX Master")
	public void user_scroll_down_until_mouse_logitech_mx_master_3_005() {
		WebUI.scrollToElement(findTestObject('Product_Mouse_Logitech_Page/Homepage/MouseLogitechMXTxtHomapage'), 0)
	}
	@When("user click product Mouse Logitech")
	public void user_click_product() {
		WebUI.click(findTestObject('Product_Mouse_Logitech_Page/Homepage/MouseLogitechMXTxtHomapage'))
	}
	@Then("screen display the product Mouse Logitech")
	public void screen_display_the_product_mouse_logitech() {
		WebUI.verifyElementVisible(findTestObject('Product_Mouse_Logitech_Page/DetailPage/MouseLogitechMXTitle'))
	}
	@Then("screen display the product Iphone 14")
	public void screen_display_the_product_iphone() {
		WebUI.verifyElementVisible(findTestObject('Product_Iphone14_Page/DetailPage/Iphone14Title'))
	}
	@And("user scroll down until Iphone 14 pro deep purple")
	public void user_scroll_down_until_iphone() {
		WebUI.scrollToElement(findTestObject('Product_Iphone14_Page/HomePage/iphone14proTxtHomepage'), 0)
	}
	@When("user click product Iphone 14")
	public void user_click_product_iphone() {
		WebUI.click(findTestObject('Product_Iphone14_Page/HomePage/iphone14proTxtHomepage'))
	}
}