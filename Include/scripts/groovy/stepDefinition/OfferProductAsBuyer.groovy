package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class OfferProductAsBuyer {


	@Then("user Click Saya tertarik dan ingin nego Mouse Logitech")
	public void user_Click_Saya_tertarik_dan_ingin_nego_mouse_logitech() {
		WebUI.click(findTestObject('Product_Mouse_Logitech_Page/DetailPage/OfferBtn'))
	}

	@Then("user Click Saya tertarik dan ingin nego Iphone 14")
	public void user_Click_Saya_tertarik_dan_in_iphone_14() {
		WebUI.click(findTestObject('Product_Iphone14_Page/DetailPage/Iphone14OfferBtn'))
	}

	@Then("screen display pop up")
	public void screen_display_pop_up() {
		WebUI.verifyElementVisible(findTestObject('Product_Mouse_Logitech_Page/DetailPage/OfferDescTxt'))
	}

	@Then("user input Harga tawar {string}")
	public void user_input_Harga_tawar(String price) {
		WebUI.setText(findTestObject('Product_Mouse_Logitech_Page/DetailPage/OfferField'), price)
	}


	@Then("user click kirim")
	public void user_click_kirim() {
		WebUI.click(findTestObject('Product_Mouse_Logitech_Page/DetailPage/sendOfferBtn'))
	}

	@Then("the button tawar change to label menunggu respon penjual")
	public void the_button_tawar_change_to_label_menunggu_respon_penjual() {
		WebUI.verifyElementNotVisible(findTestObject('Product_Mouse_Logitech_Page/DetailPage/sendOfferBtn'))
	}

	@And("user scroll down until product card")
	public void user_scroll_down_until_product_card() {
		WebUI.scrollToElement(findTestObject('Product_Page_Homepage/ProductInHomepageCard'), 0)
	}

	@When("user click product")
	public void user_click_product() {
		WebUI.click(findTestObject('Product_Page_Homepage/ProductInHomepageCard'))
	}

	@When("user click product category Kesehatan")
	public void user_click_product_category_kesehatan() {
		WebUI.click(findTestObject('HomePage/KesehatanTxtFromCard'))
	}

	@Then("screen display the product")
	public void screen_display_the_product() {
		WebUI.verifyElementVisible(findTestObject('Product_Page_Homepage/ProductTitle'))
	}


	@Then("user Click Saya tertarik dan ingin nego")
	public void user_Click_Saya_tertarik_dan_ingin_nego() {
		WebUI.click(findTestObject('Product_Page_Homepage/OfferBtn'))
	}

	@Then("User redirect to Login")
	public void user_redirect_to_Login() {
		WebUI.verifyElementVisible(findTestObject('Page_Login/AllertLogin'))
	}
}