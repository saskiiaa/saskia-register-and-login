package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class category {
	@Given("user launch the website")
	public void user_launch_the_website() {
		WebUI.openBrowser('');
		WebUI.maximizeWindow();
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/');
		WebUI.verifyElementVisible(findTestObject('HomePage/LogoSecondhand'))
	}

	@And("user scroll down until telusuri kategori Kesehatan")
	public void user_scroll_down_until_telusuri_kategori_Kesehatan() {
		WebUI.scrollToElement(findTestObject('HomePage/CategoryKesehatan'), 0)
	}

	@And("user click Category Kesehatan")
	public void user_click_Category_Kesehatan() {
		WebUI.click(findTestObject('HomePage/KesehatanCategory'))
	}

	@Then("shown product category on Kesehatan page")
	public void shown_product_category_on_Kesehatan_page() {
		WebUI.verifyElementText(findTestObject('HomePage/KesehatanTxtFromCard'), 'Kesehatan')
	}

	@And("user scroll down until telusuri semua kategori")
	public void user_scroll_down_until_telusuri_semua_kategori() {
		WebUI.scrollToElement(findTestObject('HomePage/SemuaCategory'), 0)
	}

	@And("user click semua Category")
	public void user_click_semua_Category() {
		WebUI.click(findTestObject('HomePage/SemuaCategory'))
	}

	@Then("shown product all category page")
	public void shown_product_all_category_page() {
		WebUI.verifyElementVisible(findTestObject('HomePage/semuaCatTxt'))
	}

	@And("user scroll down until telusuri kategori Hobi")
	public void user_scroll_down_until_telusuri_kategori_Hobi() {
		WebUI.scrollToElement(findTestObject('HomePage/HobiCategory'), 0)
	}

	@And("user click Category Hobi")
	public void user_click_Category_Hobi() {
		WebUI.click(findTestObject('HomePage/HobiCategory'))
	}

	@Then("shown product category on Hobi page")
	public void shown_product_category_on_Hobi_page() {
		WebUI.verifyElementText(findTestObject('HomePage/HobiTxtFromCard'), 'Hobi')
	}

	@And("user scroll down until telusuri kategori Kendaraan")
	public void user_scroll_down_until_telusuri_kategori_Kendaraan() {
		WebUI.scrollToElement(findTestObject('HomePage/KendaraanCategory'), 0)
	}

	@And("user click Category Kendaraan")
	public void user_click_Category_Kendaraan() {
		WebUI.click(findTestObject('HomePage/KendaraanCategory'))
	}

	@Then("shown product category on Kendaraan page")
	public void shown_product_category_on_Kendaraan_page() {
		WebUI.verifyElementText(findTestObject('HomePage/KendaraanTxtFromCard'), 'Kendaraan')
	}

	@And("user scroll down until telusuri kategori Baju")
	public void user_scroll_down_until_telusuri_kategori_Baju() {
		WebUI.scrollToElement(findTestObject('HomePage/BajuCategory'), 0)
	}

	@And("user click Category Baju")
	public void user_click_Category_Baju() {
		WebUI.click(findTestObject('HomePage/BajuCategory'))
	}

	@Then("shown product category on Baju page")
	public void shown_product_category_on_Baju_page() {
		WebUI.verifyElementText(findTestObject('HomePage/BajuTxtfromCard'), 'Baju')
	}

	@And("user scroll down until telusuri kategori Elektronik")
	public void user_scroll_down_until_telusuri_kategori_Elektronik() {
		WebUI.scrollToElement(findTestObject('HomePage/ElektronikCategory'), 0)
	}

	@And("user click Category Elektronik")
	public void user_click_Category_Elektronik() {
		WebUI.click(findTestObject('HomePage/ElektronikCategory'))
	}

	@Then("shown product category on Elektronik page")
	public void shown_product_category_on_Elektronik_page() {
		WebUI.verifyElementText(findTestObject('HomePage/ElektronikTxtFromCard'), 'Elektronik')
	}
}
