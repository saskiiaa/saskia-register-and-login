package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.concurrent.ConcurrentHashMap.KeySetView

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Product {
	@Given("User is page Homepage")
	public void user_is_page_homepage() {
		WebUI.openBrowser('');
		WebUI.maximizeWindow();
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/');
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/textBulanRamadhanHomepage'))
	}

	@And("User click button jual")
	public void user_click_button_jual() {
		WebUI.click(findTestObject('Page_pageProduct/buttonJualHomepage'))
	}

	@And("User redirect to page login")
	public void user_redirect_to_page_login() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/errorTextAddProductWithoutLogin'))
	}

	@And("User input cari disini {string}")
	public void user_input_cari_disini(String searchText) {
		WebUI.setText(findTestObject('Page_pageProduct/searchFieldHomepage'), searchText)
		WebUI.sendKeys(findTestObject('Page_pageProduct/searchFieldHomepage'), '\n')
	}

	@And("User click card product")
	public void user_click_card_product() {
		WebUI.click(findTestObject('Page_pageProduct/cardHuggerHomepage'))
	}

	@And("User click card product edit")
	public void user_click_card_product_edit() {
		WebUI.click(findTestObject('Page_pageProduct/AddEditProduct/cardEditProduct'))
	}

	@And("User click card product delete")
	public void user_click_card_product_delete() {
		WebUI.click(findTestObject('Page_pageProduct/AddEditProduct/cardDeleteProduk'))
	}

	@Then("User redirect to detail information screen product page")
	public void user_redirect_to_detail_information_screen_product_page() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/textDetailHugger'))
	}

	@And("User input nama product {string}")
	public void user_input_nama_product(String namaProduct) {
		WebUI.setText(findTestObject('Page_pageProduct/AddEditProduct/fieldNamaProduct'), namaProduct)
	}

	@And("User input harga product {string}")
	public void user_input_harga_product(String hargaProduct) {
		WebUI.setText(findTestObject('Page_pageProduct/AddEditProduct/fieldHargaProduct'), hargaProduct)
	}

	@And("User select kategori {string}")
	public void user_select_kategori(String kategoriProduct) {
		WebUI.selectOptionByValue(findTestObject('Page_pageProduct/AddEditProduct/selectKategoriProduct'), kategoriProduct, false)
	}

	@And("User input deskripsi {string}")
	public void user_input_deskripsi_product(String deskripsiProduct) {
		WebUI.setText(findTestObject('Page_pageProduct/AddEditProduct/fieldDeskripsiProduct'), deskripsiProduct)
	}

	@And("User upload image product {string}")
	public void user_upload_image_product(String pathImage) {
		WebUI.uploadFile(findTestObject('Page_pageProduct/AddEditProduct/uploadImageButton'), pathImage)
	}

	@And("User click button terbitkan")
	public void user_click_button_terbitkan() {
		WebUI.click(findTestObject('Page_pageProduct/AddEditProduct/buttonTerbitkanProduct'))
	}

	@And("User click button edit")
	public void user_click_button_edit() {
		WebUI.click(findTestObject('Page_pageProduct/AddEditProduct/buttonEditProduct'))
	}

	@And("User click button delete")
	public void user_click_button_delete() {
		WebUI.click(findTestObject('Page_pageProduct/AddEditProduct/buttonDeleteProduct'))
	}

	@And("User click the list product icon")
	public void user_clicks_list_product_icon() {
		WebUI.click(findTestObject('Page_pageProduct/AddEditProduct/iconLinkProdukSaya'))
	}

	@Then("User redirect to detail screen product information page")
	public void user_redirect_to_detail_screen_information_product_page() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/AddEditProduct/descDetailProductPage'))
	}

	@Then("User get validation message nama produk")
	public void user_get_validation_message_nama_produk() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/validationProduct/validationNamaProduct'))
	}

	@Then("User get validation message harga produk")
	public void user_get_validation_message_harga_produk() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/validationProduct/validationHargaProduct'))
	}

	@Then("User get validation message kategori produk")
	public void user_get_validation_message_kategori_produk() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/validationProduct/validationKategori'))
	}

	@Then("User get validation message deskripsi produk")
	public void user_get_validation_message_deskripsi_produk() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/validationProduct/validationDeskripsi'))
	}

	@Then("User redirect to list product page")
	public void user_redirect_to_list_produk_page() {
		WebUI.verifyElementVisible(findTestObject('Page_pageProduct/validationProduct/textDaftarJualSaya'))
	}
}