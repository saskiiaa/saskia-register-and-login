package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Pagination {
	@And("user scroll down")
	public void user_scroll_down() {
		WebUI.scrollToPosition(100, 2000)
		WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)
	}

	@And("user scroll down little bit")
	public void user_scroll_down_little_bit() {
		WebUI.scrollToPosition(0, 500)
		WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("there is a Next link that clickable")
	public void there_is_a_Next_link_that_clickable() {
		WebUI.verifyElementClickable(findTestObject('Aziz_Pagination/Semua_Next 1'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("there is a Previous link")
	public void there_is_a_Previous_link() {
		WebUI.verifyElementVisible(findTestObject('Aziz_Pagination/Semua_Previous 1'), FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click Next link")
	public void user_click_Next_link() {
		WebUI.click(findTestObject('Aziz_Pagination/Semua_Next 1'))
	}

	@Then("user in next page")
	public void user_in_next_page() {
		WebUI.verifyElementClickable(findTestObject('Aziz_Pagination/Semua_Previous 2'))
		WebUI.verifyElementClickable(findTestObject('Aziz_Pagination/Semua_Next 1'))
	}

	@And("User click category Baju")
	public void user_click_category_Baju() {
		WebUI.click(findTestObject('Aziz_Pagination/Cat_Baju'))
	}

	@Then("there is a Next link that clickable in category Baju")
	public void there_is_a_Next_link_that_clickable_in_category_Baju() {
		WebUI.verifyElementClickable(findTestObject('Aziz_Pagination/Semua_Next 1'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("there is a Previous link in category Baju")
	public void there_is_a_Previous_link_category_Baju() {
		WebUI.verifyElementVisible(findTestObject('Aziz_Pagination/Semua_Previous 1'), FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click Next link in category Baju")
	public void user_click_Next_link_in_category_Baju() {
		WebUI.click(findTestObject('Aziz_Pagination/Baju_Next 1'))
	}

	@Then("user in next page in category Baju")
	public void user_in_next_page_in_category_Baju() {
		WebUI.verifyElementClickable(findTestObject('Aziz_Pagination/Baju_Previous 2'))
		WebUI.verifyElementClickable(findTestObject('Aziz_Pagination/Baju_Next 1'))
	}
}