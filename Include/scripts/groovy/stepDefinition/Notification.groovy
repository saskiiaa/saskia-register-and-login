package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Notification {
	@And("user click bell icon")
	public void user_click_bell_icon() {
		WebUI.click(findTestObject('HomePage/NotificationIcn'))
	}

	@And("user click Lihat Semua Notifikasi")
	public void user_click_Lihat_Semua_Notifikasi() {
		WebUI.click(findTestObject('Page_Notification/lihatSemuaNotifikasiLink'))
	}

	@Then("user redirected to screen notification")
	public void user_redirected_to_screen_notification() {
		WebUI.verifyElementVisible(findTestObject('Page_Notification/listNotificationShow'))
	}

	@Then("user redirected to screen list of all notification")
	public void user_redirected_to_screen_list_of_all_notification() {
		WebUI.verifyElementVisible(findTestObject('Page_Notification/listNotificationScreen'))
	}

	@And("user click one of notification")
	public void user_click_one_of_notification() {
		WebUI.click(findTestObject('Page_Notification/oneNotificationLink1'))
	}

	@And("user click one of notification in screen list of all notification")
	public void user_click_one_of_notification_in_screen_list_of_all_notification() {
		WebUI.click(findTestObject('Page_Notification/oneNotificationLink2'))
	}

	@Then("user redirected to screen detail notification")
	public void user_redirected_to_screen_detail_notification() {
		WebUI.verifyElementVisible(findTestObject('Page_Notification/oneNotificationDetailScreen'))
	}
}