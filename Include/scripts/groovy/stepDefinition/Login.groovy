package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Login {

	@Given("user is on homepage")
	public void user_is_on_homepage() {
		WebUI.openBrowser('');
		WebUI.maximizeWindow();
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/');
		WebUI.verifyElementVisible(findTestObject('Page_Login/bannerText'))
	}

	@And("user click masuk button homepage")
	public void user_click_masuk_button_homepage() {
		WebUI.click(findTestObject('Page_Login/masukpageButton'))
	}



	@And("user is on login page")
	public void user_is_on_login_page() {
		WebUI.verifyElementVisible(findTestObject('Page_Login/masukText'))
	}



	@And("user input email {string}")
	public void user_input_email(String email) {
		WebUI.setText(findTestObject('Page_Login/emailField'), email)
	}



	@And("user input password {string}")
	public void user_input_password(String password) {
		WebUI.setText(findTestObject('Page_Login/passwordField'), password)
	}

	@And("user click Masuk button")
	public void user_click_Masuk_button() {
		WebUI.click(findTestObject('Page_Login/masukButton'))
		WebUI.delay(5)
		//		WebUI.verifyElementVisible(findTestObject('HomePage/searchField'))
	}

	@Then("user stay in login page")
	public void user_stay_in_login_page() {
		WebUI.verifyElementVisible(findTestObject('Page_Login/masukText'))
	}

	@Then("the screen redirect to SecondHand homepage")
	public void the_screen_redirect_to_SecondHand_homepage() {
		WebUI.verifyElementVisible(findTestObject('HomePage/ProfileIcn'))
	}
}
