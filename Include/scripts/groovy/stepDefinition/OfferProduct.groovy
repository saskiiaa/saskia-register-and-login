package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class offersProduct {
//	@And("user click bell icon")
//	public void user_click_bell_icon() {
//		WebUI.click(findTestObject('HomePage/NotificationIcn'))
//	}

	@And("user click a activity Penawaran Product")
	public void user_click_a_activity_Penawaran_Product() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/PenawaranProduk'))
	}

	@Then("user in Info Penawaran Page")
	public void user_in_Info_Penawaran_Page() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Text_Info Penawar page'))
	}

	@And("user click Terima button")
	public void user_click_Terima_button() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/label_Terima'))
	}

	@And("user click status button in Iphone 15")
	public void user_click_status_button_in_Iphone_15() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/Iphone 15_Status Button'))
	}

	@And("user click status button in Polygon")
	public void user_click_status_button_in_Polygon() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/Polygon_Status Button'))
	}

	@And("user click Berhasil Terjual checklist")
	public void user_click_Berhasil_Terjual_checklist() {
		WebUI.check(findTestObject('Aziz_InfoPenawaran_Page/Iphone 15_Berhasil terjual btn'))
	}

	@And("user click Batalkan Transaksi checklist")
	public void user_click_Batalkan_Transaksi_checklist() {
		WebUI.check(findTestObject('Aziz_InfoPenawaran_Page/Polygon_Batalkan transaksi'))
	}

	@And("user click Tolak button")
	public void user_click_Tolak_button() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/Meja_Tolak'))
	}

	@And("user click Kirim button Iphone 15")
	public void user_click_Kirim_button_Iphone_15() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/Iphone 15_Kirim'))
	}

	@And("user click Kirim button Polygon")
	public void user_click_Kirim_button_Polygon() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/Polygon_Kirim'))
	}

	@Then("there is a Contact button")
	public void there_is_a_Contact_Button() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/a_Hubungi di'))
	}

	@Then("there is Iphone 15 pro max 512GB 004")
	public void there_is_monyet_Iphone_15_pro_max_512GB_004() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Iphone 15 pro max 512GB 004'))
	}

	@Then("there is Polygon Startos S8D 003")
	public void there_is_monyet_Polygon_Startos_S8D_003() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Polygon Stratos S8D 003'))
	}

	@Then("there is Tumbler Crockcircle 16oz 002")
	public void there_is_monyet_Tumbler_Crockcircle_16oz_002() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Tumbler Crockcircle 16oz 002'))
	}

	@Then("there is Meja Kantor 001")
	public void there_is_Meja_Kantor_001() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Meja Kantor 001'))
	}

	@When("user click Contact button")
	public void user_click_Contact_button() {
		WebUI.click(findTestObject('Aziz_InfoPenawaran_Page/a_Hubungi di'))
	}

	@Then("redirect to buyer contact in WhatsApp")
	public void redirect_to_buyer_contact_in_WhatsApp() {
		WebUI.verifyElementClickable(findTestObject('Aziz_InfoPenawaran_Page/a_Hubungi di'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("there is the Perbarui status penjualan produkmu pop up Iphone 15")
	public void there_is_the_Perbarui_status_penjualan_produkmu_pop_up_Iphone_15() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Iphone 15_Popup'))
	}

	@Then("there is the Perbarui status penjualan produkmu pop up Polygon")
	public void there_is_the_Perbarui_status_penjualan_produkmu_pop_up_Polygon() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Polygon_Perbarui status penjualan produkmu'))
	}

	@Then("the status product changes to Penjualan dibatalkan")
	public void the_status_product_changes_to_Penjualan_dibatalkan() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Polygon_Penjualan dibatalkan'))
	}

	@Then("the status product changes to Berhasil terjual")
	public void the_status_product_changes_to_Berhasil_terjual() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Iphone 15_Berhasil terjual'))
	}

	@Then("the status products whose offers are reject change to Penawaran Produk ditolak")
	public void the_status_products_whose_offers_are_reject_change_to_Penawaran_Produk_ditolak() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Meja_Penawaran produk ditolak'))
	}

	@Then("the status products whose offers are accept change to Penawaran produk diterima")
	public void the_status_products_whose_offers_are_accept_change_to_Penawaran_produk_diterima() {
		WebUI.verifyElementVisible(findTestObject('Aziz_InfoPenawaran_Page/Tumbler_Penawaran produk diterima'))
	}
}
