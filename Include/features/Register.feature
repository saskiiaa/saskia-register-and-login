@Register

Feature: Register
	@Registerusingcorrectemailpassword
	Scenario: User want to register using correct email, password
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user clik daftar link
		And user input username "ningning"
		And user input emailreg "emailRandom"
		And user input password "karina"
		And user clik Daftar button
		Then the screen redirect to SecondHand homepage
		
	@Registerusingexistedemail
	Scenario: User want to register existed email
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user clik daftar link
		And user input username "ning"
		And user input emailreg "existingEmail"
		And user input password "karina"
		And user clik Daftar button
		Then user stay in register page
		
	@Registerusingexistedusername
	Scenario: User want to register using existed username
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user clik daftar link
		And user input username "ningning"
		And user input emailreg "emailRandom"
		And user input password "karina"
		And user clik Daftar button
		Then the screen redirect to SecondHand homepage
		
	@Registerusinginvalidemailcharacter
	Scenario: User want to register using invalid email character
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user clik daftar link
		And user input username "ningningnn"
		And user input emailreg "invalidemail"
		And user input password "karina"
		And user clik Daftar button
		Then user stay in register page
		
	@Registerusingpasswordwithonecharacter
	Scenario: User want to register using password with one character
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user clik daftar link
		And user input username "nini"
		And user input emailreg "emailRandom"
		And user input password "k"
		And user clik Daftar button
		Then the screen redirect to SecondHand homepage
		
	@Registerusingallemptyfields
	Scenario: User want to register with all empty fields
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page	
		And user clik daftar link
		And user clik Daftar button
		Then user stay in register page
	