@OfferProduct @Nadiah
Feature: Offer Product

@BuyerOfferProduct
  Scenario: User want to offer the product 
   Given user is on homepage
   And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until product card
    When user click product 
    Then screen display the product 
    And user Click Saya tertarik dan ingin nego 
    Then screen display pop up
    And user input Harga tawar "12000"
		And user click kirim
		Then the button tawar change to label menunggu respon penjual

  @BuyerOfferProductfromSearch
  Scenario: User want to offer the product with search product
   Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    When user click search field
    And user input Product "Mouse Logitech MX Master"
    And user click enter
    And user scroll down until Mouse Logitech MX Master
    And List Product display the product
    When user click product Mouse Logitech
    Then screen display the product Mouse Logitech
    And user Click Saya tertarik dan ingin nego Mouse Logitech
    Then screen display pop up
		And user input Harga tawar "1200000"
		And user click kirim
		Then the button tawar change to label menunggu respon penjual
		

  @BuyerOfferProductNoPrice
  Scenario: User want to offer the product with no Price
   Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    When user click search field
    And user input Product "Iphone 14 pro deep purple"
    And user click enter
    And user scroll down until Iphone 14 pro deep purple
    And List Product display the product
    When user click product Iphone 14
    Then screen display the product Iphone 14
    And user Click Saya tertarik dan ingin nego Iphone 14
    Then screen display pop up
		And user click kirim
		Then the button tawar change to label menunggu respon penjual
		
		@BuyerOfferProductWithNoLogin
  Scenario: User want to offer the product not logged in yet
   Given user is on homepage
    And user scroll down until product card
    When user click product 
    Then screen display the product 
    And user Click Saya tertarik dan ingin nego 
    Then screen display pop up
		And user click kirim
		Then User redirect to Login
		
