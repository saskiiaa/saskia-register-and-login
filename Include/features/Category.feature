@Category @Nadiah

Feature: Category

	@AllCategorywitoutLogin
		Scenario: see product by All Category witout Login
		Given user is on homepage
    And user scroll down until telusuri semua kategori 
    And user click semua Category 
    Then shown product all category page
    
	@CategoryKesehatanwitoutLogin
		Scenario: see product by category kesehatan witout Login
		Given user is on homepage
    And user scroll down until telusuri kategori Kesehatan
    And user click Category Kesehatan
    Then shown product category on Kesehatan page
    
    @CategoryHobiwitoutLogin
		Scenario: see product by category Hobi witout Login
		Given user is on homepage
    And user scroll down until telusuri kategori Hobi
    And user click Category Hobi
    Then shown product category on Hobi page
    
    @CategoryKendaraanwitoutLogin
		Scenario: see product by category Kendaraan witout Login
		Given user is on homepage
    And user scroll down until telusuri kategori Kendaraan
    And user click Category Kendaraan
    Then shown product category on Kendaraan page
    
    @CategoryBajuwitoutLogin
		Scenario: see product by category Baju witout Login
		Given user is on homepage
    And user scroll down until telusuri kategori Baju
    And user click Category Baju
    Then shown product category on Baju page
    
    @CategoryElektronikwitoutLogin
		Scenario: see product by category Elektronik witout Login
		Given user is on homepage
    And user scroll down until telusuri kategori Elektronik
    And user click Category Elektronik
    
    @AllCategoryWithLogin
		Scenario: see product by All kesehatan with login
		Given user is on homepage
		And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until telusuri semua kategori 
    And user click semua Category 
    Then shown product all category page
    
    @CategoryKesehatanWithLogin
		Scenario: see product by category kesehatan with login
		Given user is on homepage
		And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until telusuri kategori Kesehatan
    And user click Category Kesehatan
    Then shown product category on Kesehatan page
    
    @CategoryHobiWithLogin
		Scenario: see product by category Hobi with login
		Given user is on homepage
		 And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until telusuri kategori Hobi
    And user click Category Hobi
    Then shown product category on Hobi page
    
    @CategoryKendaraanWithLogin
		Scenario: see product by category Kendaraan with login
		Given user is on homepage
		 And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until telusuri kategori Kendaraan
    And user click Category Kendaraan
    Then shown product category on Kendaraan page
    
    @CategoryBajuWithLogin
		Scenario: see product by category Baju with login
		Given user is on homepage
		 And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until telusuri kategori Baju
    And user click Category Baju
    Then shown product category on Baju page
    
    @CategoryElektronikWithLogin
		Scenario: see product by category Elektronik with login
		Given user is on homepage
		 And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    And user scroll down until telusuri kategori Elektronik
    And user click Category Elektronik
    Then shown product category on Elektronik page