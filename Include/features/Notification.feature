@Notification @Velia

Feature: Notification
		
	@ShowNotification
	Scenario: User want to view notification
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click bell icon
		Then user redirected to screen notification
	
	@ShowDetailNotification
	Scenario: User want to view detail notification
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click bell icon
		Then user redirected to screen notification
		And user click one of notification
		Then user redirected to screen detail notification
		
	@ShowListOfAllNotification
	Scenario: User want to view list of all notification
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click bell icon
		Then user redirected to screen notification
		And user click Lihat Semua Notifikasi
		Then user redirected to screen list of all notification
		
	@ShowDetailNotificationViaAlllNotification
	Scenario: User want to view detail notification via all notification screen
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click bell icon
		Then user redirected to screen notification
		And user click Lihat Semua Notifikasi
		Then user redirected to screen list of all notification
		And user click one of notification in screen list of all notification
		Then user redirected to screen detail notification
		
		