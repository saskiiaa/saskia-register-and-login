@Search @Nadiah
Feature: Search

  @SearchProductWithLogin
  Scenario: User want to search product with login
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    When user click search field
    And user input Product "Mouse Logitech MX Master"
    And user click enter
    And user scroll down until Mouse Logitech MX Master
    Then List Product display the product
	
	@SearchProductDetailWithLogin
  Scenario: User want to see detail product after search product with login
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"
    And user input password "kelompok2"
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    When user click search field
    And user input Product "Mouse Logitech MX Master"
    And user click enter
    And user scroll down until Mouse Logitech MX Master
    And List Product display the product
    When user click product Mouse Logitech
    Then screen display the product Mouse Logitech
    
   @SearchProductNoLogin
  Scenario: User want to search product without Login
    Given user is on homepage
    When user click search field
    And user input Product "Mouse Logitech MX Master"
    And user click enter
    And user scroll down until Mouse Logitech MX Master
    Then List Product display the product
    
 @SearchProductDetailWithNoLogin
  Scenario: User want to see detail product after search product without login
    Given user is on homepage
    When user click search field
    And user input Product "Mouse Logitech MX Master"
    And user click enter
    And user scroll down until Mouse Logitech MX Master
    And List Product display the product
    When user click product Mouse Logitech
    Then screen display the product Mouse Logitech