@Profile @Velia

Feature: Profile

	@UpdateProfileUsingCorrectData
	Scenario: User want to update profile using correct data
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
		And user upload image 'D:\image\item-images-6.jpg'
		And user input name "kelompok 2 binar"
		And user select city "Jakarta"
		And user input address "Jalan nomor 20"
		And user input phone number "081366765644"
		And user click button simpan
		Then user redirected to screen homepage 
		
	@UpdateProfileUsingEmptyNamaField
	Scenario: User want to update profile using empty name field
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
		And user input name ""
		And user click button simpan
		Then user stay in profile page
		
	@UpdateProfileUsingEmptyKotaField
	Scenario: User want to update profile using empty kota field
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
		And user select city "Pilih Kota"
		And user click button simpan
		Then user stay in profile page
		
	@UpdateProfileUsingEmptyAlamatField
	Scenario: User want to update profile using empty alamat field
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
		And user input address ""
		And user click button simpan
		Then user stay in profile page
	
	@UpdateProfileUsingEmptyPhoneNumberField
	Scenario: User want to update profile using empty phone number field
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
		And user input phone number ""
		And user click button simpan
		Then user stay in profile page
		
	@ViewProfile
	Scenario: User want to view profile
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
	
	@ReturnToHomePage
	Scenario: User want to return to the screen homepage while user on the screen profile page
		Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "buyerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		Then user redirected to screen homepage
		And user click profile icon
		And user click list profile button
		Then screen profile page displayed
		And user click button back profile
		Then user redirected to screen homepage
		
		