@page
Feature: pagination

  @pageAll
  Scenario: All Category Pagination
    Given user is on homepage
			And user scroll down
			Then there is a Next link that clickable
			Then there is a Previous link
			And user click Next link
			Then user in next page
			
	 @pageClothes
  Scenario: Clothes Category Pagination
    Given user is on homepage
    	And user scroll down little bit
    	And User click category Baju
			And user scroll down
			Then there is a Next link that clickable in category Baju
			Then there is a Previous link in category Baju
			And user click Next link in category Baju
			Then user in next page in category Baju