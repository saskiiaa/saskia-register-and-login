@sellProduct
  Feature: Product
  
    @showAllSellProduct
    Scenario: Show all sell product
     	Given user is on homepage
			And user click masuk button homepage
			And user is on login page
			And user input email "sellerkelompok2@gmail.com"	
			And user input password "kelompok2"	
			And user click Masuk button	
      And click list product icon
      Then redirect to sell screen product page
  
    @showInterestedInProduct
    Scenario: Show intersted in product
      Given user is on homepage
			And user click masuk button homepage
			And user is on login page
			And user input email "sellerkelompok2@gmail.com"	
			And user input password "kelompok2"	
			And user click Masuk button	
      And click list product icon
      Then redirect to sell screen product page
      And click button Diminati
      Then displays screen product page for products that are of interest to buyers
      
    @ShowSoldProduct
    Scenario: Show Sold product
      Given user is on homepage
			And user click masuk button homepage
			And user is on login page
			And user input email "sellerkelompok2@gmail.com"	
			And user input password "kelompok2"	
			And user click Masuk button	
      And click list product icon
      Then redirect to sell screen product page
      And click button Terjual
      Then there is Iphone 15 pro max