@Product @Dicki
Feature: Product

  @AddProductWithoutLoginToSystem
  Scenario: User add product without login
    Given User is page Homepage
    And User click button jual
    Then User redirect to page login

  @ViewDetailProductFromHomepage
  Scenario: User view detail product from homepage
  	Given User is page Homepage
  	And User input cari disini "Hugger"
  	And User click card product
  	Then User redirect to detail information screen product page
  	
  @AddNewProduct
  Scenario: User want to add product with logged to system
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		And User click button jual
		And User input nama product "Kursi Gaming 006"
		And User input harga product "1400000"
		And User select kategori "1"
		And User input deskripsi "Kursi ini sedang promo akhir tahun, segera dibeli"
		And User upload image product "/Users/dickirizki/Downloads/kursiGaming.jpg"
		And User click button terbitkan
		Then User redirect to detail screen product information page
		
	@AddNewProductBlankNamaProduk
  Scenario: User want to add product with blank field Nama Produk and get notification validation below input field Nama Produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"	
		And user click Masuk button
		And User click button jual
		And User input harga product "1400000"
		And User select kategori "1"
		And User input deskripsi "Kursi ini sedang promo akhir tahun, segera dibeli"
		And User upload image product "/Users/dickirizki/Downloads/kursiGaming.jpg"
		And User click button terbitkan
		Then User get validation message nama produk
		
	@AddNewProductBlankHargaProduk
  Scenario: User want to add product with blank field Harga Produk and get notification validation below input field Harga Produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"		
		And user click Masuk button
		And User click button jual
		And User input nama product "Kursi Gaming 006"
		And User select kategori "1"
		And User input deskripsi "Kursi ini sedang promo akhir tahun, segera dibeli"
		And User upload image product "/Users/dickirizki/Downloads/kursiGaming.jpg"
		And User click button terbitkan
		Then User get validation message harga produk
		
	@AddNewProductBlankKategoriProduk
  Scenario: User want to add product with blank field Kategori Produk and get notification validation below input field Kategori Produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"		
		And user click Masuk button
		And User click button jual
		And User input nama product "Kursi Gaming 006"
		And User input harga product "1400000"
		And User input deskripsi "Kursi ini sedang promo akhir tahun, segera dibeli"
		And User upload image product "/Users/dickirizki/Downloads/kursiGaming.jpg"
		And User click button terbitkan
		Then User get validation message kategori produk
		
	@AddNewProductBlankDeskripsiProduk
  Scenario: User want to add product with blank field Deskripsi Produk and get notification validation below input field Deskripsi Produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"			
		And user click Masuk button
		And User click button jual
		And User input nama product "Kursi Gaming 006"
		And User input harga product "1400000"
		And User select kategori "1"
		And User upload image product "/Users/dickirizki/Downloads/kursiGaming.jpg"
		And User click button terbitkan
		Then User get validation message deskripsi produk
	
	@EditProduct
  Scenario: User want to Edit Product
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"			
		And user click Masuk button
		And User click the list product icon
		And User click card product edit 
		And User click button edit
		And User upload image product "/Users/dickirizki/Downloads/meja.jpeg"
		And User click button terbitkan
		Then User redirect to detail screen product information page
	
	@EditProductWithBlankNamaProduk
  Scenario: User want to Edit Product blank nama produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"		
		And user click Masuk button
		And User click the list product icon
		And User click card product edit 
		And User click button edit
		And User input nama product ""
		And User click button terbitkan
		Then User get validation message nama produk
		
	@EditProductWithBlankHargaProduk
  Scenario: User want to Edit Product blank harga produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"		
		And user click Masuk button
		And User click the list product icon
		And User click card product edit 
		And User click button edit
		And User input harga product ""
		And User click button terbitkan
		Then User get validation message harga produk
	
	@EditProductWithBlankDeskripsiProduk
  Scenario: User want to Edit Product blank deskripsi produk
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"			
		And user click Masuk button
		And User click the list product icon
		And User click card product edit 
		And User click button edit
		And User input deskripsi ""
		And User click button terbitkan
		Then User get validation message deskripsi produk
		
	@DeleteProduk
  Scenario: User want to Delete Product
  	Given user is on homepage
		And user click masuk button homepage
		And user is on login page
		And user input email "sellerkelompok2@gmail.com"	
		And user input password "kelompok2"		
		And user click Masuk button
		And User click the list product icon
		And User click card product delete 
		And User click button delete
		
		
		