@Login

Feature: Login

  @Loginwithstandarduser
  Scenario: User want to login using correct email and password  
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2@gmail.com"  
    And user input password "kelompok2"  
    And user click Masuk button
    Then the screen redirect to SecondHand homepage
    
  @Loginwithunregisteredemail
  Scenario: User want to login using unregistered email
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2345@gmail.com"  
    And user input password "kelompok2"  
    And user click Masuk button
    Then user stay in login page
    
  @Loginwithuncorrectpassword
  Scenario: User want to register using uncorrect password
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2345@gmail.com"  
    And user input password "sabi"  
    And user click Masuk button
    Then user stay in login page
    
  @Loginwithoutinputemail
  Scenario: User want to login without input email
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input password "sabi"  
    And user click Masuk button
    Then user stay in login page
  
  @Loginwithoutinputpassword
  Scenario: User want to login without input password
    Given user is on homepage
    And user click masuk button homepage
    And user is on login page
    And user input email "buyerkelompok2345@gmail.com"  
    And user click Masuk button
    Then user stay in login page