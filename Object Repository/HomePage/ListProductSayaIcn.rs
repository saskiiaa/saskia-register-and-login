<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ListProductSayaIcn</name>
   <tag></tag>
   <elementGuidId>0589d5ee-c1e8-497f-9337-f96ae7489b80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.bi.bi-list-ul.me-4.me-lg-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li/a/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>019f777b-50f6-44ec-94b7-bdee093119d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bi bi-list-ul me-4 me-lg-0</value>
      <webElementGuid>014d3e1a-d535-40f8-b3f4-373af6be98e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5&quot;]/a[@class=&quot;nav-link d-flex align-items-center&quot;]/i[@class=&quot;bi bi-list-ul me-4 me-lg-0&quot;]</value>
      <webElementGuid>2de933c7-20f5-40dc-854f-55560d3c07c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li/a/i</value>
      <webElementGuid>fede2a0c-6632-4b21-aefe-0a23a8c07463</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/i</value>
      <webElementGuid>4bc3b3d7-d6e9-49e5-a5af-25666c67d265</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
