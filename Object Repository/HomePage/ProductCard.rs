<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ProductCard</name>
   <tag></tag>
   <elementGuidId>f32c2100-fcee-4370-920b-7eeebc429fcd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-body.text-decoration-none</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div/a/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>09393b71-8939-4f4a-9887-0948d87a5fc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body text-decoration-none</value>
      <webElementGuid>8f109238-8b16-4a77-a2cd-bf0650db3ce3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      phone
      Elektronik
      Rp 1.200.000
    </value>
      <webElementGuid>678faa2a-c610-41b6-9721-61c0c55757c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]</value>
      <webElementGuid>14e3e346-a746-4ede-958e-4afbc83bae7e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div/a/div/div</value>
      <webElementGuid>b7201021-eb40-4484-a90c-464a97d70af8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/a/div/div</value>
      <webElementGuid>5eeddb0d-68cf-4108-81fe-2506ae6f38b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      phone
      Elektronik
      Rp 1.200.000
    ' or . = '
      phone
      Elektronik
      Rp 1.200.000
    ')]</value>
      <webElementGuid>6bdb8664-ffd0-4de3-90b7-b690966609a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
