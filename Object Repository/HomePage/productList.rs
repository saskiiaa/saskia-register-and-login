<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productList</name>
   <tag></tag>
   <elementGuidId>6f3df727-df67-496e-88eb-b8d76d987672</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#products</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b63b4f99-a154-49d2-b1d4-c4c32915012a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row my-5 g-5</value>
      <webElementGuid>58304d61-d569-4181-8c1b-45b643f8d7a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>products</value>
      <webElementGuid>f87442f9-b68c-4c6d-b5ae-17310486b495</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
        
  
      

    
      phone
      Elektronik
      Rp 1.200.000
    
  

      
      
        
  
      

    
      Elektronik
      Elektronik
      Rp 2.000.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      

    ← Previous Next →
  </value>
      <webElementGuid>1852e9c2-9cd9-44e3-b887-004e46769a28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)</value>
      <webElementGuid>26e714e4-a2a0-4329-ac2a-4a57fffef737</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='products']</value>
      <webElementGuid>f1390672-e3bb-43eb-8ce7-0d6099cc005b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[2]/div</value>
      <webElementGuid>5999b852-2b3a-4034-8e01-79fdc68c9837</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'products' and (text() = '
      
        
  
      

    
      phone
      Elektronik
      Rp 1.200.000
    
  

      
      
        
  
      

    
      Elektronik
      Elektronik
      Rp 2.000.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      

    ← Previous Next →
  ' or . = '
      
        
  
      

    
      phone
      Elektronik
      Rp 1.200.000
    
  

      
      
        
  
      

    
      Elektronik
      Elektronik
      Rp 2.000.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      
      
        
  
      

    
      Kucing Geprek
      Hobi
      Rp 1.000
    
  

      

    ← Previous Next →
  ')]</value>
      <webElementGuid>a13b816c-5d04-42aa-bf17-11ba92ce5d91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
