<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textDaftarJualSaya</name>
   <tag></tag>
   <elementGuidId>a01dfec6-736a-4e89-b01a-1ae3703fd429</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.fw-bold.my-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/section/div/h3</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/h3[@class=&quot;fw-bold my-5&quot;][count(. | //*[(text() = 'Daftar Jual Saya' or . = 'Daftar Jual Saya')]) = count(//*[(text() = 'Daftar Jual Saya' or . = 'Daftar Jual Saya')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>f4be0d2d-4e7f-45e8-96a2-ac0bc4c7cd84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold my-5</value>
      <webElementGuid>336655c2-eaf9-4d66-84fd-888f0f9d0fb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Jual Saya</value>
      <webElementGuid>48f64e49-8de4-4789-bdd1-85dce4e79edf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/h3[@class=&quot;fw-bold my-5&quot;]</value>
      <webElementGuid>11788508-421c-4dfe-963e-9347f9fe8096</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product was successfully destroyed.'])[1]/following::h3[1]</value>
      <webElementGuid>91bf8bef-1b1b-4a3e-abef-3248d3c564f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Omdik21'])[2]/preceding::h3[1]</value>
      <webElementGuid>121c3b4e-46f6-456d-a5e3-b110ef1dee50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[2]/preceding::h3[1]</value>
      <webElementGuid>92afbd40-eeff-4d36-b7d3-82b0d581a5f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Jual Saya']/parent::*</value>
      <webElementGuid>3205d2ec-5e81-4724-a00b-cda90826389d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>8c11c841-4f34-4636-a0eb-f992ffdbf479</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Daftar Jual Saya' or . = 'Daftar Jual Saya')]</value>
      <webElementGuid>a59ceffc-446f-440f-8734-53a47d97614f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
