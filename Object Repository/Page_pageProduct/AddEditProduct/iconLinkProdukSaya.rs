<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconLinkProdukSaya</name>
   <tag></tag>
   <elementGuidId>38029738-0554-41b9-941d-b18e8462488b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;navbarSupportedContent&quot;]/div/ul/li[1]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.d-flex.align-items-center.active</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5&quot;]/a[@class=&quot;nav-link d-flex align-items-center active&quot;][count(. | //*[@href = '/products']) = count(//*[@href = '/products'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8fb4698a-2ee4-4b6e-8961-e5fdabf682e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex align-items-center active</value>
      <webElementGuid>d3626749-a83c-4bdc-98ff-49ae3260b452</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products</value>
      <webElementGuid>ac46fe46-ecdf-4843-9d1a-bbb0ecd0f01e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
      Produk Saya
</value>
      <webElementGuid>0e298b4e-4d59-4ebc-a885-247289da699e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5&quot;]/a[@class=&quot;nav-link d-flex align-items-center active&quot;]</value>
      <webElementGuid>954ef882-9e4d-4c3f-a4ae-8de587a5db15</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li/a</value>
      <webElementGuid>e0f086b9-30a2-493a-bc89-225a0c15579b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/preceding::a[1]</value>
      <webElementGuid>9e549029-34ca-4815-93d7-f9c4f6a98819</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products')]</value>
      <webElementGuid>fe96b05a-132f-4b62-91bb-97a98c15673b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>22d4b79c-054c-44b3-9b62-ca029b8f1007</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products' and (text() = '
      
      Produk Saya
' or . = '
      
      Produk Saya
')]</value>
      <webElementGuid>15a4ca04-8ec9-4a33-90fb-afaee8be4e03</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
