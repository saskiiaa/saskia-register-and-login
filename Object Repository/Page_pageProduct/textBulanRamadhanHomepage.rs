<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textBulanRamadhanHomepage</name>
   <tag></tag>
   <elementGuidId>1130e11f-5607-4d41-a451-cf0f0f253aff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.fw-bold.mb-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>97b2b9dd-ab76-4b91-9c1e-33658e6d55d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>0b65a722-7cf3-4e9c-86e3-69fda2d73d21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan RamadhanBanyak diskon!</value>
      <webElementGuid>2efdaf6c-ee30-48f9-84f2-3663e847538d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;mt-lg-5 mb-5 w-100 d-flex gap-lg-5&quot;]/div[@class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;]/div[@class=&quot;row p-5&quot;]/div[@class=&quot;col&quot;]/h1[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>c1c52e4a-f934-46e6-8d28-9f0770bc2d4f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      <webElementGuid>3672166f-f978-421c-99f9-823f0c3fcef1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/preceding::h1[1]</value>
      <webElementGuid>e3d0a1b5-9cfb-4163-aaab-8d9e818233cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan Ramadhan']/parent::*</value>
      <webElementGuid>c40accac-b150-4fa4-9d03-3fe4d8353638</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>459ffbc2-ce8b-46d6-b80d-d47653e7c506</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Bulan RamadhanBanyak diskon!' or . = 'Bulan RamadhanBanyak diskon!')]</value>
      <webElementGuid>6eaa15de-d52b-4aa5-b774-356596e2849a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
