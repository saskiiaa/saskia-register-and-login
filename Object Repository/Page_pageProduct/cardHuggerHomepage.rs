<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cardHuggerHomepage</name>
   <tag></tag>
   <elementGuidId>030a2368-2d4e-4fab-9c80-5beabf7db7da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div/a/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card.px-0.border-0.shadow.h-100.pb-4.rounded-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8ae825ae-a4d0-4d82-b1e9-20ecb18176c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>d629eadf-413c-49d2-9f80-bdf2ec29ef91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      Hugger Honda Airbl...
      Kendaraan
      Rp 200.000
    
  </value>
      <webElementGuid>145333ac-9a16-4508-aea7-2a7f39476235</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>fcae4332-8c54-42d1-9488-1027c18cce69</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div/a/div</value>
      <webElementGuid>b2fe4de5-8717-4f6c-b1a7-2a4a3d485f2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>f0b13146-ebeb-40dc-802f-71d9063764d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      Hugger Honda Airbl...
      Kendaraan
      Rp 200.000
    
  ' or . = '
      

    
      Hugger Honda Airbl...
      Kendaraan
      Rp 200.000
    
  ')]</value>
      <webElementGuid>48d0e6b4-3955-4bb9-b145-833290ab220e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
