<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>validationDeskripsi</name>
   <tag></tag>
   <elementGuidId>3a8f67d4-2477-40ad-a523-b11c16e33e25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/section/section/section/div/form/div[4]/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a70d1362-fb32-4330-8146-f424619d694c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text text-danger</value>
      <webElementGuid>5d76377a-7aca-4a93-99c3-f32c81e1728c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Category must exist
          </value>
      <webElementGuid>13c41399-c6ea-4a50-b3e2-0646e2c92fe9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container container-sm position-relative&quot;]/section[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-11&quot;]/form[1]/div[@class=&quot;mb-4&quot;]/div[@class=&quot;form-text text-danger&quot;]</value>
      <webElementGuid>a0f96fba-2d4d-40b8-aba1-2703c9232b01</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::div[1]</value>
      <webElementGuid>f1253c3b-9c18-4596-9933-f6c50c43de46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Price can', &quot;'&quot;, 't be blank')])[1]/following::div[2]</value>
      <webElementGuid>b05d1768-9bdd-4ac4-98bc-ec357449aa0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/preceding::div[1]</value>
      <webElementGuid>d3dd69c8-d828-4805-9fd3-e0538271c559</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Description can', &quot;'&quot;, 't be blank')])[1]/preceding::div[3]</value>
      <webElementGuid>818af0c5-a4a9-4c90-86ef-41785aebed7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Category must exist']/parent::*</value>
      <webElementGuid>e716dab5-6e20-47b5-ab39-84f90439257e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>e61d6bb6-a588-4d02-80c8-c92697b0f226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Category must exist
          ' or . = '
            Category must exist
          ')]</value>
      <webElementGuid>6a12cc1d-d8bb-436b-99c6-9ba43a633d0d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
