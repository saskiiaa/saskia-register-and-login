<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorTextAddProductWithoutLogin</name>
   <tag></tag>
   <elementGuidId>bee42ed1-c8eb-493e-bf3b-4dcd266edc2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.alert-dismissible.position-fixed.top-0.start-50.translate-middle-x.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e69d09e3-ebea-4419-8d7a-34520457f4d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5</value>
      <webElementGuid>f8ebd4e0-a509-4eb1-bf84-8dd6fc93f346</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>e0c8e33f-350e-4fbc-ad2e-dff621fdc946</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        You need to sign in or sign up before continuing.
        
      </value>
      <webElementGuid>e6051cb5-9457-42b6-85d5-425357e70cf9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5&quot;]</value>
      <webElementGuid>fcd941e0-5726-40c4-a1bc-d070d20d651d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      <webElementGuid>2ce2e6bf-8046-44cf-b3f5-ea4658e92d04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/preceding::div[2]</value>
      <webElementGuid>cb490bd4-bfc6-46e8-b045-06fab6f01bdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You need to sign in or sign up before continuing.']/parent::*</value>
      <webElementGuid>ab0ba90e-5c3a-43e2-bf47-2bb0f505d2d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>e83e3acf-1fea-47ae-8b46-b4a55bd2f5a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        You need to sign in or sign up before continuing.
        
      ' or . = '
        You need to sign in or sign up before continuing.
        
      ')]</value>
      <webElementGuid>b5ef5e85-4be4-4ee1-8927-1a4f0f5d6dd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
