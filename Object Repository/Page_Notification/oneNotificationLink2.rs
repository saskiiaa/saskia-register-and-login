<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>oneNotificationLink2</name>
   <tag></tag>
   <elementGuidId>d08a527b-623a-4b38-9233-267e29e3143f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li > a.notification.my-4.px-2.position-relative</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = '/products/63808']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/section/section/ul/li[1]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a5555563-c72a-45ee-bc13-07791f7be9ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>ca2e3c0b-0b30-4e2b-8bd5-7c33c45ac6f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>106781</value>
      <webElementGuid>55ce3201-52b0-499b-b7e3-a0031b26ca48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>33dcbff8-85a5-4b1a-bcdc-5190d0c67933</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products/63808</value>
      <webElementGuid>d5101fe3-f3f1-465f-92b6-7bf095dacdb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
            
              Berhasil di terbitkan
              Motor Vario 160Rp 13.000.000
            
            17 Nov, 21:59

</value>
      <webElementGuid>6745badc-d724-4fb8-856a-040e64d1d6de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>3835c517-a0d7-4566-bdd3-0ca6fe9d9083</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::a[1]</value>
      <webElementGuid>c1e462ff-f410-4e13-a3b6-0202b4215f96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/products/63808')])[2]</value>
      <webElementGuid>3fdb97b1-c15e-475e-a94a-339f02c8eaec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li/a</value>
      <webElementGuid>6f63e6d5-0421-4506-98d3-c67b26a26bdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/63808' and (text() = '
              
            
              Berhasil di terbitkan
              Motor Vario 160Rp 13.000.000
            
            17 Nov, 21:59

' or . = '
              
            
              Berhasil di terbitkan
              Motor Vario 160Rp 13.000.000
            
            17 Nov, 21:59

')]</value>
      <webElementGuid>5a3cd9da-5c48-4549-8c73-eff8d7573908</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
