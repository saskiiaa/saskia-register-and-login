<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lihatSemuaNotifikasiLink</name>
   <tag></tag>
   <elementGuidId>93ec77fa-6a96-410b-810c-26489594d668</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-link.text-decoration-none</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/notifications']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;navbarSupportedContent&quot;]/div/ul/li[3]/ul/li[4]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>38971f38-670c-43f0-aadd-46732ae60006</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link text-decoration-none</value>
      <webElementGuid>c879045f-e823-4bfb-a6a7-0365a3b155c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>87d36c32-e15a-491d-a18f-c71fdd9b3c1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Lihat semua notifikasi
</value>
      <webElementGuid>0da0df68-fa3e-462c-9db8-de6a7dcbb551</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[@class=&quot;p-2 text-center&quot;]/a[@class=&quot;btn btn-link text-decoration-none&quot;]</value>
      <webElementGuid>8ce881f1-ce2a-4e0f-9154-86aa140a9911</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li[4]/a</value>
      <webElementGuid>68ee0a68-97d5-4673-92b4-e6b20d0ea682</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat semua notifikasi')]</value>
      <webElementGuid>db811d12-9dde-40e3-a8de-606eaab43029</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ipad 9 Gray BlueRp 60.000.000'])[1]/following::a[1]</value>
      <webElementGuid>773a6a17-f734-4681-bc72-4c5317e7b981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[2]/following::a[1]</value>
      <webElementGuid>3721f21a-d3e4-4b1d-abc7-9edce94dfef1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[1]/preceding::a[1]</value>
      <webElementGuid>e3f6d66d-8d85-4da0-b63a-ad855131f1f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/preceding::a[2]</value>
      <webElementGuid>aad9a7be-e1c0-40fc-80a8-43cc7277d698</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lihat semua notifikasi']/parent::*</value>
      <webElementGuid>88e7f5b7-f3b9-43f0-b900-3e627c8c0075</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/notifications')])[2]</value>
      <webElementGuid>92478b4e-ab2e-4d90-b430-135b187ca718</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li[4]/a</value>
      <webElementGuid>8f1f80d9-b21d-44cb-b233-0d367f9e1a38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
                Lihat semua notifikasi
' or . = '
                Lihat semua notifikasi
')]</value>
      <webElementGuid>613d538a-1365-4b59-8ea4-47216869b830</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
