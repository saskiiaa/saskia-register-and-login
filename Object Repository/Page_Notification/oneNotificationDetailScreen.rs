<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>oneNotificationDetailScreen</name>
   <tag></tag>
   <elementGuidId>8ef8a764-425e-486e-a5c1-739a35e2fcb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>section.pt-5.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::section[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>1a58cd99-86fb-4acd-8e9d-a96a34812730</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pt-5 mt-5</value>
      <webElementGuid>ee83447b-7920-480b-9e28-1a39e6704987</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
  
    
        

          
              
                
              
          

        

      
        
          Deskripsi
          Vario 160 warna hitam
        
      

    

    
      
        
          Motor Vario 160
          Kendaraan
          Rp 13.000.000
            Edit
            Delete
        
      

      
        
          

          
            kelompok 2 binar
            Jakarta
          
        
      
    
  



    </value>
      <webElementGuid>ba524267-0bb7-4fa8-a962-d1c9f4d0deff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]</value>
      <webElementGuid>35df901b-1ed0-4457-a067-a8bed2c5ecfd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::section[1]</value>
      <webElementGuid>d2b3d155-b9fc-4119-be1b-d39f8ac668e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>c50becf7-9fae-46a0-8e79-939d99a3c694</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
      
  
    
        

          
              
                
              
          

        

      
        
          Deskripsi
          Vario 160 warna hitam
        
      

    

    
      
        
          Motor Vario 160
          Kendaraan
          Rp 13.000.000
            Edit
            Delete
        
      

      
        
          

          
            kelompok 2 binar
            Jakarta
          
        
      
    
  



    ' or . = '
      
  
    
        

          
              
                
              
          

        

      
        
          Deskripsi
          Vario 160 warna hitam
        
      

    

    
      
        
          Motor Vario 160
          Kendaraan
          Rp 13.000.000
            Edit
            Delete
        
      

      
        
          

          
            kelompok 2 binar
            Jakarta
          
        
      
    
  



    ')]</value>
      <webElementGuid>de74c2cc-fd87-4c4e-b41b-04b7efe865c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
