<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>listNotificationShow</name>
   <tag></tag>
   <elementGuidId>dda7d52f-3183-4b3b-94ca-048a315b963c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.dropdown-menu.notification-dropdown-menu.px-4.show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>b74e901d-3c79-4219-9897-d42013aca27c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-menu notification-dropdown-menu px-4 show</value>
      <webElementGuid>574d3be8-88c5-4b60-badf-b942b20b5672</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-popper</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>f6ad49ab-0281-4be7-96a5-2bfd2e6b2763</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          
            
                
              
                Berhasil di terbitkan
                Motor Vario 160Rp 13.000.000
              
              17 Nov, 21:59

          

            
          
            
                
              
                Berhasil di terbitkan
                Ipad 9 Gray BlueRp 60.000.000
              
              17 Nov, 21:57

          

            
              
                Lihat semua notifikasi
            
    </value>
      <webElementGuid>c082e8a6-53be-4c32-a37b-14420ff07ae6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]</value>
      <webElementGuid>42ba5644-d761-4ce9-bef0-c2bdfd799bdd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul</value>
      <webElementGuid>e13f7fc0-8b61-4a09-81a7-aae441e002c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/following::ul[1]</value>
      <webElementGuid>dae9cb34-7e02-4678-86e5-2efea925b4b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::ul[1]</value>
      <webElementGuid>22b699ef-b178-4a37-8a08-a4fe55089a07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul</value>
      <webElementGuid>7a3c8999-4195-47b3-b864-bb3bb0dac9c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
          
            
                
              
                Berhasil di terbitkan
                Motor Vario 160Rp 13.000.000
              
              17 Nov, 21:59

          

            
          
            
                
              
                Berhasil di terbitkan
                Ipad 9 Gray BlueRp 60.000.000
              
              17 Nov, 21:57

          

            
              
                Lihat semua notifikasi
            
    ' or . = '
          
            
                
              
                Berhasil di terbitkan
                Motor Vario 160Rp 13.000.000
              
              17 Nov, 21:59

          

            
          
            
                
              
                Berhasil di terbitkan
                Ipad 9 Gray BlueRp 60.000.000
              
              17 Nov, 21:57

          

            
              
                Lihat semua notifikasi
            
    ')]</value>
      <webElementGuid>fb776ff2-a47e-4a32-8f2b-6189efc7518b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
