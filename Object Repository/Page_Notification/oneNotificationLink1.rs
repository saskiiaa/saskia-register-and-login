<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>oneNotificationLink1</name>
   <tag></tag>
   <elementGuidId>200f7257-0d23-43ce-8f56-c74ceae82f2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.notification.my-4.px-2.position-relative</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;navbarSupportedContent&quot;]/div/ul/li[3]/ul/li[1]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4c80fb19-c5e3-4225-9aa2-775b343f7a88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>7d47a286-e625-40d9-9938-9a40284859c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>106781</value>
      <webElementGuid>e2658198-6f1d-4761-b6d8-4c45d757c4f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3b3d46da-0dbb-4bd4-b54e-df93cb46589b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products/63808</value>
      <webElementGuid>76f3957f-e5b0-4a61-9fbb-ce4e9abe3653</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
              
                Berhasil di terbitkan
                Motor Vario 160Rp 13.000.000
              
              17 Nov, 21:59

</value>
      <webElementGuid>bae92ab2-8d00-471b-8508-0c1c037ffee3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>335eaf58-010a-4ffa-98ec-20721c01c87a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li/a</value>
      <webElementGuid>c5aefd73-155d-4b5b-a483-bbb30a20ccb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/following::a[2]</value>
      <webElementGuid>79bc6322-5bdf-42b6-bb61-d658f5bdb0a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::a[3]</value>
      <webElementGuid>7562478e-2f18-41a3-97c2-131edad27a78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products/63808')]</value>
      <webElementGuid>f1673c3a-4380-4732-aeaf-2428ad20012a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li/a</value>
      <webElementGuid>fe95347b-a04f-4bab-948c-9984df1c86a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/63808' and (text() = '
                
              
                Berhasil di terbitkan
                Motor Vario 160Rp 13.000.000
              
              17 Nov, 21:59

' or . = '
                
              
                Berhasil di terbitkan
                Motor Vario 160Rp 13.000.000
              
              17 Nov, 21:59

')]</value>
      <webElementGuid>0872e9c7-7a53-4c65-936b-c78ab1d9ea81</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
