<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Polygon_Popup</name>
   <tag></tag>
   <elementGuidId>c2445b22-c0d8-4a5a-800c-b23af08811ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>form.modal-content.px-4.py-2</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//form[@action = '/offers/52346' and (text() = '
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
' or . = '
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='/offers/52346']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content px-4 py-2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>/offers/52346</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept-charset</name>
      <type>Main</type>
      <value>UTF-8</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal52346&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='/offers/52346']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal52346']/div/form</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::form[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::form[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
   </webElementXpaths>
</WebElementEntity>
