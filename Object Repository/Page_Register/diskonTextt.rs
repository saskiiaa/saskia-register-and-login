<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>diskonTextt</name>
   <tag></tag>
   <elementGuidId>80459ce1-7e18-4d99-bb40-f4b35c555e3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.fw-bold.mb-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>23858200-a3f1-4b9d-a696-d8eafb24326a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>40b9ad59-dc38-49a2-935d-bc6736eb08d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan RamadhanBanyak diskon!</value>
      <webElementGuid>389b4fea-1489-4ccc-acd5-2f0a4ea6947f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;mt-lg-5 mb-5 w-100 d-flex gap-lg-5&quot;]/div[@class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;]/div[@class=&quot;row p-5&quot;]/div[@class=&quot;col&quot;]/h1[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>4878f6e2-5666-43db-871c-1aeb71232551</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      <webElementGuid>b7bdd797-3c32-4e40-a623-ebb32b3ce7aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/preceding::h1[1]</value>
      <webElementGuid>e5af064e-4cf0-4444-8d8a-b3f39322a920</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan Ramadhan']/parent::*</value>
      <webElementGuid>104c00e0-de07-4164-ba75-9dd3573ea5f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>b603781d-45b2-480f-8093-899a5e6f4c6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Bulan RamadhanBanyak diskon!' or . = 'Bulan RamadhanBanyak diskon!')]</value>
      <webElementGuid>a3706a89-2bee-4a62-92a2-7b4d515ed83a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
