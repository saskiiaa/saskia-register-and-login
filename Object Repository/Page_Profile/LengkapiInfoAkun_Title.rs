<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LengkapiInfoAkun_Title</name>
   <tag></tag>
   <elementGuidId>7d418c71-1e9d-4af1-a512-e7611f96a409</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
        Lengkapi Info Akun
      ' or . = '
        Lengkapi Info Akun
      ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.fs-6.fw-bold.text-center.position-absolute.top-50.start-50.translate-middle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5984afc2-da12-460e-9d26-d95827ca0c89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle</value>
      <webElementGuid>f21961dc-7664-4565-9684-5cc43022152f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Lengkapi Info Akun
      </value>
      <webElementGuid>0ff8fa82-9115-4a39-bf2f-af8bdac2f9f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle&quot;]</value>
      <webElementGuid>eef5123a-d90d-4dac-a81c-8a917c6ea771</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      <webElementGuid>2661167f-6273-42d1-971e-c2a0d2236226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/preceding::div[6]</value>
      <webElementGuid>7549b05e-6ee8-490f-9cd9-e0fb6b37244a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lengkapi Info Akun']/parent::*</value>
      <webElementGuid>11eca222-8c31-43ba-b601-07b9d78cffb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>8f3070fe-85ef-49ae-9170-55347071ac66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Lengkapi Info Akun
      ' or . = '
        Lengkapi Info Akun
      ')]</value>
      <webElementGuid>5dd03dbc-ecbe-456c-968d-84cdbeec463b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
