<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alamatField</name>
   <tag></tag>
   <elementGuidId>0a3d0540-23af-4285-ba9f-3c8193233cd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='user_address']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#user_address</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'user[address]' and @id = 'user_address']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>8b88a5af-e9b7-4ce4-abb3-e6c509d227d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>139842be-60c0-4bca-8097-97f1091f84c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: Jalan Ikan Hiu 33</value>
      <webElementGuid>63bad63c-33cc-4655-97d3-45045a422e94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>38ae124e-fdd7-4e77-b02d-342963da6444</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user[address]</value>
      <webElementGuid>7004cdd4-135e-4183-a44f-421e3f89d3ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_address</value>
      <webElementGuid>079ada24-3bca-4f0a-871a-424c6af4d8b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>jalabn raya jakarta ibukota</value>
      <webElementGuid>3adef0ed-0943-4ac8-b10b-adee48182871</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user_address&quot;)</value>
      <webElementGuid>841134c5-af9b-4b38-816c-66ff7a054e42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='user_address']</value>
      <webElementGuid>dff64074-1bf3-4a2b-bc11-74d601e4af2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat'])[1]/following::textarea[1]</value>
      <webElementGuid>fb77d92d-9982-4a6c-a0da-3d86fb8d1b74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/following::textarea[1]</value>
      <webElementGuid>feb57993-4616-4dbb-af52-6cc18cfaa0c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No Handphone'])[1]/preceding::textarea[1]</value>
      <webElementGuid>3a2c85fe-84c4-41a6-991d-a9e7f3d2445e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::textarea[1]</value>
      <webElementGuid>b285c78d-6cac-44e8-a65d-d3a62fe09222</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='jalabn raya jakarta ibukota']/parent::*</value>
      <webElementGuid>2b250eab-e4a5-443e-a513-1417ab326e8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>3665496c-6c06-44a1-9a45-efd38a2d3f33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Contoh: Jalan Ikan Hiu 33' and @name = 'user[address]' and @id = 'user_address' and (text() = 'jalabn raya jakarta ibukota' or . = 'jalabn raya jakarta ibukota')]</value>
      <webElementGuid>bad4931e-8a2e-4536-ae05-28f94bece9a3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
