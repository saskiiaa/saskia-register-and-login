<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bannerText</name>
   <tag></tag>
   <elementGuidId>dde6b1e0-4fad-4224-b6b5-d558ae30bce4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Bulan RamadhanBanyak diskon!' or . = 'Bulan RamadhanBanyak diskon!')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.fw-bold.mb-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>889b4c58-5c34-4065-aa6a-a77e06545400</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>f07830b2-e86c-4ae0-99d3-0266a0ef5ba6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan RamadhanBanyak diskon!</value>
      <webElementGuid>64fc4dd5-0a97-4b0c-8132-a49a08658d06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;mt-lg-5 mb-5 w-100 d-flex gap-lg-5&quot;]/div[@class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;]/div[@class=&quot;row p-5&quot;]/div[@class=&quot;col&quot;]/h1[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>2c75cbec-1e50-497e-9cbe-49e5a9e5bf26</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      <webElementGuid>d95f8913-b00d-44f6-8c3d-66a3af4ba3a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/preceding::h1[1]</value>
      <webElementGuid>132a65a6-b2f2-4930-a68e-599d27f32f98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan Ramadhan']/parent::*</value>
      <webElementGuid>117a7ff9-d409-436f-acc4-47b7e1f517c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>637439ce-8ea1-4189-8f98-6dbf99cb2b3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Bulan RamadhanBanyak diskon!' or . = 'Bulan RamadhanBanyak diskon!')]</value>
      <webElementGuid>184b076c-442d-491e-8f1f-863ca399ee14</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
