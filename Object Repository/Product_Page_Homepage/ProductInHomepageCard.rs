<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ProductInHomepageCard</name>
   <tag></tag>
   <elementGuidId>948755de-29cc-4cbb-b87a-ded2ce8c3121</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div[5]/a/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>01b2c3eb-9d3b-4650-8d90-b8ba1cb0d53f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body text-decoration-none</value>
      <webElementGuid>e3361d91-1068-474c-a2b6-74c63d2fa68c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      Kucing Geprek
      Hobi
      Rp 1.000
    </value>
      <webElementGuid>099d7145-2139-4244-8e6f-af690dac5536</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]</value>
      <webElementGuid>2a766244-01cf-483c-a038-432d9fff25d4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div[5]/a/div/div</value>
      <webElementGuid>2959de6d-f767-42fe-a343-1a945a48dc07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 1.000'])[3]/following::div[3]</value>
      <webElementGuid>83f3285d-9ec7-478f-b658-c5ca3ff00c3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kucing Geprek'])[3]/following::div[3]</value>
      <webElementGuid>d70d0ac1-8d1c-40af-b42d-2cf2b7fa6658</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a/div/div</value>
      <webElementGuid>7446d716-8998-492e-a0aa-76bf633e22dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      Kucing Geprek
      Hobi
      Rp 1.000
    ' or . = '
      Kucing Geprek
      Hobi
      Rp 1.000
    ')]</value>
      <webElementGuid>e49371a8-1780-4a59-85a4-58bcb6a58635</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
