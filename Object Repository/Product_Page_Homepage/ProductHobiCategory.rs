<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ProductHobiCategory</name>
   <tag></tag>
   <elementGuidId>eea9bde4-a675-4531-bcd8-2b386887370e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div/a/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body.text-decoration-none</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>48b399d4-3ff8-483b-bac9-f5a83d5e4908</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body text-decoration-none</value>
      <webElementGuid>6d40aaf1-b218-488b-91d0-3001d36d65a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      Kucing Geprek
      Hobi
      Rp 1.000
    </value>
      <webElementGuid>7278eb3b-a70b-4fd2-a72e-47e01f61b659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]/div[@class=&quot;card-body text-decoration-none&quot;]</value>
      <webElementGuid>a59671bd-dd90-409a-979a-3e961149ef64</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div/a/div/div</value>
      <webElementGuid>0e793748-730e-491f-9f3f-d076dccbb398</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>77aa7009-71ae-4f00-9041-4cff79ea6037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      Kucing Geprek
      Hobi
      Rp 1.000
    ' or . = '
      Kucing Geprek
      Hobi
      Rp 1.000
    ')]</value>
      <webElementGuid>c94d4f18-2edd-4e38-ba64-f80733681a98</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
