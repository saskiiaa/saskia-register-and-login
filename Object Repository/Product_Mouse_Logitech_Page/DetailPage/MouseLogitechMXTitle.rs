<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MouseLogitechMXTitle</name>
   <tag></tag>
   <elementGuidId>d48e9f84-a547-484a-b4fa-b01829f33f1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::h5[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h5.card-title.fs-5.fw-bolder</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]/h5[@class=&quot;card-title fs-5 fw-bolder&quot;][count(. | //*[(text() = 'Mouse Logitech MX Master 3 005 ' or . = 'Mouse Logitech MX Master 3 005 ')]) = count(//*[(text() = 'Mouse Logitech MX Master 3 005 ' or . = 'Mouse Logitech MX Master 3 005 ')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>cea5a8e3-7d74-4e83-b3d6-059063c9a41d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title fs-5 fw-bolder</value>
      <webElementGuid>792cf110-2ad7-49fb-a0a5-7ed361bb98a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mouse Logitech MX Master 3 005 </value>
      <webElementGuid>e2212ede-3dad-4c8d-ba66-2193811f0cf0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]/h5[@class=&quot;card-title fs-5 fw-bolder&quot;]</value>
      <webElementGuid>ed48d9c0-a715-4509-ae39-72b4d8e5d5e1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::h5[1]</value>
      <webElementGuid>16e1593c-f0fb-4111-ba10-817f38c4013d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/preceding::h5[1]</value>
      <webElementGuid>799ef0c4-b7b5-465e-8c1a-e97b7c7a43eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Seller Kelompok 2'])[1]/preceding::h5[1]</value>
      <webElementGuid>1f1ca3ec-e7c8-489a-8262-fb74a0df2eb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Mouse Logitech MX Master 3 005']/parent::*</value>
      <webElementGuid>7d1f3a52-ff2a-48ad-b695-34072884f264</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/h5</value>
      <webElementGuid>7b80520e-44bf-4b3f-93da-f658ec270369</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Mouse Logitech MX Master 3 005 ' or . = 'Mouse Logitech MX Master 3 005 ')]</value>
      <webElementGuid>54944987-9818-4f75-b1d2-3d7c780339b6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
